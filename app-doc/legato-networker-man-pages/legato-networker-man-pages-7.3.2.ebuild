# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Legato Backup Suite - man pages"
HOMEPAGE="http://software.emc.com/products/software_az/networker.htm?hlnav=T"
SRC_URI="ftp://ftp.legato.com/pub/eval/2006Q3/nw732_linux_x86.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="app-arch/rpm2targz"
RDEPEND=""

src_install() {
	cd ${WORKDIR} || die

	rpm2tar "lgtoman-7.3.2-1.i686.rpm" || die
	tar xf "lgtoman-7.3.2-1.i686.tar" -C ${D} || die
}
